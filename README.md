# Tauri-CI

image for ci (test & build) for the tauri projects

frontend: `yarn build` or `npm build`\
backend: `cd src-tauri; cargo build`
