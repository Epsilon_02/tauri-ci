FROM ubuntu:20.04

LABEL Author 6543@obermui.de, epsilon_02@noreply.codeberg.org

RUN useradd --uid 1001 --create-home runner

RUN apt-get update
RUN apt-get install tzdata -y --quiet
RUN apt-get install keyboard-configuration -y --quiet

# install build dependencys
RUN apt-get install -y --quiet libwebkit2gtk-4.0-dev build-essential curl wget libssl-dev

# install node14
RUN curl -sL https://deb.nodesource.com/setup_14.x | bash - && apt-get update && apt-get install -y nodejs && npm install -g yarn

# install rust toolchain + needed binaries
USER runner
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y --profile minimal --component clippy rustfmt
ENV PATH="/home/runner/.cargo/bin:${PATH}"
